using JuMP, AmplNLWriter, Bonmin_jll

# -1 = value not given
# 0 or 1 = value given
# Example puzzle from https://www.binarypuzzle.com
tab = [ -1 -1  1 -1 -1 -1;
         0  0 -1  1 -1 -1;
         0 -1 -1 -1 -1 -1;
        -1 -1 -1 -1 -1 -1;
        -1 -1 -1  1 -1 -1;
        -1 -1 -1 -1  0 -1]

a = size(tab)[1]
b = size(tab)[2]

m = Model(() -> AmplNLWriter.Optimizer(Bonmin_jll.amplexe))

@variable(m, x[1:a,1:b], Bin)

@objective(m, Min, sum(x[i,j] for j=1:b, i=1:a))


# Values should be the same as the ones from tab
for i = 1:a, j = 1:b
    if tab[i,j] != -1
        @constraint(m, x[i,j] == tab[i,j])
    end
end



# No three consecutive values in a line should be the same
for i = 1:a, j = 1:b-2
    @constraint(m, sum(x[i,k] for k=j:j+2) >= 1)
    @constraint(m, sum(x[i,k] for k=j:j+2) <= 2)
end

# No three consecutive values in a column should be the same
for i = 1:a-2, j = 1:b
    @constraint(m, sum(x[k,j] for k=i:i+2) >= 1)
    @constraint(m, sum(x[k,j] for k=i:i+2) <= 2)
end

# Each line should have the same number of 0 and 1
for i = 1:a
    @constraint(m, sum(x[i,j] for j=1:b) == b/2)
end

# Each column should have the same number of 0 and 1
for j = 1:b
    @constraint(m, sum(x[i,j] for i=1:a) == a/2)
end

# Each line should be unique
for i = 1:a, k=i+1:a
    @NLconstraint(m, sum((x[i,j] - x[k,j])^2 for j=1:b) >= 0)
end

# Each column should be unique
for j = 1:b, k=j+1:b
    @NLconstraint(m, sum((x[i,j] - x[i,k])^2 for i=1:a) >= 0)
end


println(m)

optimize!(m)
@show solution_summary(m, verbose=true)


for i = 1:a, j = 1:b
    tab[i,j] = value(x[i,j])
end

for i = 1:a
    for j = 1:b
        print(tab[i,j])
        print(' ')
    end
    println()
end
